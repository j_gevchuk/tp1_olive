<?php

	session_start();
	//require_once("../modeles/accessbd.php");
		
	//Si l'utilisateur clique sur Deconnexion
	if(isset($_GET["deconnexion"]))
	{
		//Supprimer la session et revenir à la page d'accueil
		unset($_SESSION);
		session_destroy();
		header ('Location: Accueil.php');
	}
	
	//Variable contenant les messages concernant la connexion
	$message = "";
	
	
	/*** Connexion sécurisée avec MD5 et le gran de sel ***/
	if(!isset($_SESSION["grainSel"]))
	{
		//générer le grain de sel côté serveur
		//nécéssaire pour obtenir md5(md5(password) + grainSel)
		$grainSel = rand(1, 1000);
		$_SESSION["grainSel"] = $grainSel;
	}
	
	//lorsque l'on reçoit l'information du formulaire
	if(isset($_POST["txtsLogin"]) && isset($_POST["txtsPass"]))
	{
		//echo $_POST["txtsLogin"] . " " . $_POST["txtsPass"];
		//Connexion à la base de données
		laConnexion();
		
		$resultats = laSelection("select id, nom, prenom, password, admin from clients where courriel = '" . mysql_real_escape_string($_POST["txtsLogin"]) . "'");
		
		//si y'a une rangée avec ce username
		if($resultat = mysql_fetch_array($resultats))
		{
			//on a déjà le mot de passe MD5 dans $resultat["password"]
			$passDB = $resultat["password"];
			$passDBEncrypte = md5($passDB . $_SESSION["grainSel"]);
			//echo " " . $passDBEncrypte . " " . $_SESSION["grainSel"];
			
			if($passDBEncrypte === $_POST["txtsPass"])
			{
				//Garder l'ID, le nom et le prenom de l'utilisateur pour les réutiliser dans les autres pages	
				$_SESSION["user"] = $resultat["id"];
				$_SESSION["prenom"] = $resultat["prenom"];
				$_SESSION["nom"] = $resultat["nom"];
				if($resultat["admin"] === "1")
				{
					$_SESSION["admin"] = $resultat["admin"];
				}
				header("Location: Produits_Oho.php");
			}
			else
			{
				//Mot de passe invalide ou autre login existant mais pas le bon mot de passe
				$message = "Combinaison invalide!";
			}			
		}
		else
		{
			//Login innexistant
			$message = "Combinaison invalide!";		
		}
	}

?>

<script type="text/javascript">
	// <![CDATA[	
		//Fonction pour encoder le mot de passe envoyer et l'envoyer au serveur
		function encodeEtEnvoi()
		{
			//Réupérer les valeurs du formulaire de connexion
			var username = document.forms["formConnexion"].txtsLogin.value;
			var password = document.forms["formConnexion"].txtsPass.value;
			var grainSel = document.forms["formConnexion"].grainSel.value;
			
			//Crypter le mot de passe
			var passEncode = MD5(MD5(password) + grainSel);
			
			//Remplir les champs hidden pour un envoi en POST puis soumettre le formulaire
			document.forms["formSoumissionConnexion"].txtsLogin.value = username;
			document.forms["formSoumissionConnexion"].txtsPass.value = passEncode;
			document.forms["formSoumissionConnexion"].submit();

		}
		
		//Gérer le message d'erreur pour le formulaire de connexion
		function estString(valeur)
		{
			if(!isNaN(valeur))
			{
				document.getElementById("err_txtsLogin").innerHTML = "*";
				document.getElementById("lbl_txtsLogin").style.fontWeight="bold";
				document.getElementById("lbl_txtsLogin").style.color="red";
				return false;
			} else {
				document.getElementById("err_txtsLogin").innerHTML = "";
				document.getElementById("lbl_txtsLogin").style.fontWeight="normal";
				document.getElementById("lbl_txtsLogin").style.color="#1B3613";
				return true;
			}
		}
		
		function envoyer()
		{
			//Faire la validation du formulaire de connexion
			if(estString(document.forms["formConnexion"].txtsLogin.value) && (validerForm(document.forms["formConnexion"].elements))) encodeEtEnvoi();
		}
	// ]]>
</script>

<div id="haut">
	<a href="Accueil.php" ><img src="medias/logo/logo.png" alt="Huile d'OHo" /></a>
	<?php include_once 'vues/panierLien.php' ?>

	<!-- Connexion -->
	<div id="connexion">
		<?php
			if(!isset($_SESSION["user"]))
			{
				?>
					<!-- Un lien pour le formulaire d'inscription -->
					<h4>Vous êtes déjà <a href="?page=inscription">inscrit</a>?</h4>
					<!--<h4>Vous êtes déjà <a href="vues/Fiche_Oho.php">inscrit</a>?</h4>-->
					<form name="formConnexion" method="post" action="">
						<div class="zone">	
							<label id="lbl_txtsLogin">Courriel :  </label>
							<input type="text" name="txtsLogin"/>
							<div class="errConnexion" id="err_txtsLogin"></div>	
						</div>
						
						<div class="zone">	
							<label id="lbl_txtsPass">Mot de passe : </label>
							<input type="password" name="txtsPass"/>
							<div class="errConnexion" id="err_txtsPass"></div>
						</div>
						
						<div class="zone">
							<div class="vide">
							<input type="hidden" name="grainSel" value="<?php echo $_SESSION["grainSel"];?>"/>	
							</div>
							<input type="button" onclick="envoyer();" value="Connexion"/>
						</div>
					</form>
					
					<!-- Envoyer les information de connexion avec MD5 -->
					<form name="formSoumissionConnexion" method="post" action="">
						<input type="hidden" name="txtsLogin"/>
						<input type="hidden" name="txtsPass"/>
					</form>
					
					<div class="zone">	
					<div class="vide"></div>
					<div class="errMessage"><h4><?php echo $message;?></h4></div>	
					</div>
				<?php
								
			} else {
			//Si l'utilisateur est connecté, lui afficher un message personnalisé (nom et prenom) et générer un lien pour qu'il ait accès a son compte ou se 
			// deconnecter
				?>	
					<div id="autehntificationDiv">
						<!--<h4>Bienvenue <?php echo $_SESSION["prenom"]. " " . $_SESSION["nom"]; ?>!</h4>-->
						<h4>Votre <a href="Fiche_Oho.php">compte</a></h4>
						<?php if (isset ($_SESSION["admin"])) { ?> <h4><a href="Administrateur.php">Admin</a></h4> <?php }?>
						<div class="vide"></div>
						<h4><a href="index.php?deconnexion">Deconnexion</a></h4>
							
					</div>
				<?php
				}
				?>
	</div>
</div>
			