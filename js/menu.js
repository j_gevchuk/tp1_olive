/*Fonctions pour g�rer le css du menu en fonction de la page courante*/
	
	//R�cup�rer les onbjets par leur id et modifier leur couleur leur background image et leur text selon la page
	//M�me principe pour toutes les fonctions suivantes
	
	
	function menuAccueil(){
	
		var accueilVar = document.getElementById("accueil");
		var oliviersVar = document.getElementById("oliviers");
		var contactVar = document.getElementById("contact");
		var produitsVar = document.getElementById("produits");
		var connexionVar = document.getElementById("connexion");
		
		accueilVar.style.backgroundImage="url('medias/menu/menuActif.png')";
		/*accueilVar.style.backgroundRepeat="no-repeat";*/
		accueilVar.style.fontWeight="bold";
		accueilVar.style.color="#ffffff";
		
		oliviersVar.style.backgroundImage="url('medias/menu/menuPassif.png')";
		oliviersVar.style.fontWeight="normal";
		oliviersVar.style.color="#1B3613";
		
		produitsVar.style.backgroundImage="url('medias/menu/menuPassif.png')";
		produitsVar.style.fontWeight="normal";
		produitsVar.style.color="#1B3613";
		
		/*connexionVar.style.background="#c7d24c";
		connexionVar.style.fontWeight="normal";
		connexionVar.style.color="#1B3613";*/
		
		contactVar.style.backgroundImage="url('medias/menu/menuPassif.png')";
		contactVar.style.fontWeight="normal";
		contactVar.style.color="#1B3613";
	}

	function menuOliviers(){
		var accueilVar = document.getElementById("accueil");
		var oliviersVar = document.getElementById("oliviers");
		var contactVar = document.getElementById("contact");
		var produitsVar = document.getElementById("produits");
		var connexionVar = document.getElementById("connexion");
		
		accueilVar.style.backgroundImage="url('medias/menu/menuPassif.png')";
		accueilVar.style.fontWeight="normal";
		accueilVar.style.color="#1B3613";
		
		oliviersVar.style.backgroundImage="url('medias/menu/menuActif.png')";
		oliviersVar.style.fontWeight="bold";
		oliviersVar.style.color="#ffffff";
		
		produitsVar.style.backgroundImage="url('medias/menu/menuPassif.png')";
		produitsVar.style.fontWeight="normal";
		produitsVar.style.color="#1B3613";
		
		/*connexionVar.style.background="#c7d24c";
		connexionVar.style.fontWeight="normal";
		connexionVar.style.color="#1B3613";*/
		
		contactVar.style.backgroundImage="url('medias/menu/menuPassif.png')";
		contactVar.style.fontWeight="normal";
		contactVar.style.color="#1B3613";
	}

	function menuContact(){
		var accueilVar = document.getElementById("accueil");
		var oliviersVar = document.getElementById("oliviers");
		var contactVar = document.getElementById("contact");
		var produitsVar = document.getElementById("produits");
		var connexionVar = document.getElementById("connexion");
		
		accueilVar.style.backgroundImage="url('medias/menu/menuPassif.png')";
		accueilVar.style.fontWeight="normal";
		accueilVar.style.color="#1B3613";
		
		oliviersVar.style.backgroundImage="url('medias/menu/menuPassif.png')";
		oliviersVar.style.fontWeight="normal";
		oliviersVar.style.color="#1B3613";
		
		produitsVar.style.backgroundImage="url('medias/menu/menuPassif.png')";
		produitsVar.style.fontWeight="normal";
		produitsVar.style.color="#1B3613";
		
		/*connexionVar.style.background="#c7d24c";
		connexionVar.style.fontWeight="normal";
		connexionVar.style.color="#1B3613";*/
		
		contactVar.style.backgroundImage="url('medias/menu/menuActif.png')";
		contactVar.style.fontWeight="bold";
		contactVar.style.color="#ffffff";

	}

	function menuProduits(){
		var accueilVar = document.getElementById("accueil");
		var oliviersVar = document.getElementById("oliviers");
		var contactVar = document.getElementById("contact");
		var produitsVar = document.getElementById("produits");
		var connexionVar = document.getElementById("connexion");
		
		accueilVar.style.backgroundImage="url('medias/menu/menuPassif.png')";
		accueilVar.style.fontWeight="normal";
		accueilVar.style.color="#1B3613";
		
		oliviersVar.style.backgroundImage="url('medias/menu/menuPassif.png')";
		oliviersVar.style.fontWeight="normal";
		oliviersVar.style.color="#1B3613";
		
		produitsVar.style.backgroundImage="url('medias/menu/menuActif.png')";
		produitsVar.style.fontWeight="bold";
		produitsVar.style.color="#ffffff";
		
		/*connexionVar.style.background="#c7d24c";
		connexionVar.style.fontWeight="normal";
		connexionVar.style.color="#1B3613";*/
		
		contactVar.style.backgroundImage="url('medias/menu/menuPassif.png')";
		contactVar.style.fontWeight="normal";
		contactVar.style.color="#1B3613";
	}

	function menuConnexion(){
		var accueilVar = document.getElementById("accueil");
		var oliviersVar = document.getElementById("oliviers");
		var contactVar = document.getElementById("contact");
		var produitsVar = document.getElementById("produits");
		var connexionVar = document.getElementById("connexion");
		
		accueilVar.style.backgroundImage="url('medias/menu/menuPassif.png')";
		accueilVar.style.fontWeight="normal";
		accueilVar.style.color="#1B3613";
		
		oliviersVar.style.backgroundImage="url('medias/menu/menuPassif.png')";
		oliviersVar.style.fontWeight="normal";
		oliviersVar.style.color="#1B3613";
		
		produitsVar.style.backgroundImage="url('medias/menu/menuPassif.png')";
		produitsVar.style.fontWeight="normal";
		produitsVar.style.color="#1B3613";
		
		/* connexionVar.style.background="#1B3613";
		connexionVar.style.fontWeight="bold";
		connexionVar.style.color="#ffffff"; */
		
		contactVar.style.backgroundImage="url('medias/menu/menuPassif.png')";
		contactVar.style.fontWeight="normal";
		contactVar.style.color="#1B3613";
	}
