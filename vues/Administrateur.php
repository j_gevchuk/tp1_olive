<?php ob_start(); ?>
<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	
<html xmlns="http://www.w3.org/1999/xhtml">

		
		<head>
			<!-- Fonts et ASCII -->
			<?php
				include_once("php/fonts.htm");
			?>
			<title>Administrateur - Les oliviers Ould Hocine</title>
			<meta name="robots" content="noindex, nofollow" /> 
		
			<!-- Appels CSS et javascript -->
			<?php
				include_once("php/cssETjs.htm");
			?>
			<!--liens menant vers les scripts CSS et javascript-->
			<script src="js/swfobject_modified.js" type="text/javascript"></script>
		</head>
		
<body class="corps">
	<div id="contenu">
		
		
		<!-- Entete -->
		<?php
			include_once("php/entete.php");
		?>
		
		
		<!-- Le menu -->
		<?php
			include_once("php/menu.htm");
		?>		
		
		<!-- Contenu pprincipal -->
		<div id="contenuPrincipal">	
			<?php
				if ((isset($_SESSION["admin"])) && (isset($_SESSION["user"]))) { 
					include_once("php/admin.php");
				} else {
					header("Location: Accueil.php");
				}
			?>
		</div>		
		
		<!-- Bas de page -->
		<?php
			include_once("php/reseaux.htm");
			include_once("php/piedPage.htm");
		?>		
	</div>
	
	<?php
		include_once("php/jQuery.htm");
	?>
	<script  type="text/javascript">
		// <![CDATA[
			menuConnexion();
		// ]]>
	</script>
</body>
</html>
<?php ob_end_flush(); ?> 