<?php 
/**
 * Fichier: 'vues.php'
 * * @author  Iulianna Gevchuk
 * Description: Vues
 *
 */

	class Vues {
		public function accueil() {
			include_once('vues/Accueil.php');
		}

		public function oliviers() {
			include_once('vues/Olive_Oho.php');
		}

		public function contacts() {
			include_once('vues/Contacts_Oho.php');
		}

		public function planSite() {
			include_once('vues/plan.php');
		}

		public function produits($produits){
			include_once('vues/Produits_Oho.php');
		}

		public function panier(){
			include_once 'vues/panier.php';
		}
	}

 ?>