<!-- Afficher le lien vers le panier -->
<div class='panier'>
	<?php 
	//Lorsque panier contient au moins 1 article, le lien est actif
		if(isset($_SESSION['panier']) && count($_SESSION['panier']) > 0){
			echo "<a href='?page=panier'>Panier(".count($_SESSION['panier']).")</a>";
		}
		else {
			echo "<a href=''>Panier</a>";
		}
	?>
</div>