<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	
<html xmlns="http://www.w3.org/1999/xhtml">

		
		<head>
			<!-- Fonts et ASCII -->
			<?php
				include_once("php/fonts.htm");
			?>
			<title>Plan du site - Les Oliviers Ould Hocine</title>
			<meta name="description" content="Plan du site web des Oliviers Ould Hocine" />
			<meta name="keywords" content="Kabylie, Olive, Huile, Algérie, Tradition, Ould Hocine, Bouira, Oliviers" /> 
			<meta name="robots" content="index, follow" /> 
			<meta name="author" content="Lilia Ould Hocine" />
		
			<!-- Appels CSS et javascript -->
			<?php
				include_once("php/cssETjs.htm");
			?>
		</head>
		
<body class="corps">
	<div id="contenu">
		
		<!-- Entete -->
		<?php
			include_once("php/entete.php");
		?>
		
		
		<!-- Le menu -->
		<?php
			include_once("php/menu.htm");
		?>		
		
		<!-- Contenu pprincipal -->
		<div id="contenuPrincipal">	
			<h1>Plan du site</h1>
			<h2>Tous les utilisateurs</h2>
			<div id="plan">
			<ul>
				<li><a href="Accueil.php">La page d'accueil</a></li>
				<li><a href="Olive_Oho.php">Les Oliviers O'Ho</a></li>
				<li><a href="Produits.php">Les produits O'Ho</a></li>
				<li><a href="Produits_Oho.php?panier">Le panier</a></li>
				<li><a href="Contact_Oho.php">Pour nous contacter</a></li>
				<li><a href="Fiche_Oho.php">Pour s'inscrire</a></li>
				<?php
					if(isset($_SESSION["user"]))
					{
						echo "<li>";
						?>
						
						<h2>Pour les utilisateurs connectcés</h2>
						<ul>
							<li><a href="Fiche_Oho.php">Compte personnel</a></li>
							<li><a href="Produits_Oho.php?panier">Passer les commandes</a></li>
												
						<?php
							if(isset($_SESSION["admin"]))
							{
								echo "<li>";
								?>
									<h2>Pour l'administrateur</h2>	
									<ul>
										<li><a href="Administrateur.php">La page de l'administrateur</a></li>
										<li><a href="Administrateur.php?entree=1">Les nouvelles commandes (Les nouvelles commandes non traitées)</a></li>
										<li><a href="Administrateur.php?entree=2">Gérer les adminitrateurs (Ajouter nouvel administrateur, enlever les droits d'administration)</a></li>
										<li><a href="Administrateur.php?entree=3">Gérer les produits (Ajouter nouveau produit, enlever ou remettre un produit)</a></li>
										<li><a href="Administrateur.php?entree=4">Cherche l'historique d'un client</a></li>
									</ul>
								<?php
								echo "</li>";
							}
						echo "</ul></li>";
					}
				
				?>
				
			</ul>
			</div>
		</div>		
		
		<!-- Bas de page -->
		<?php		
			include_once("php/reseaux.htm");
			include_once("php/piedPage.htm");
		?>		
	</div>
	
	<?php
		include_once("php/jQuery.htm");
	?>
	<script  type="text/javascript">
		// <![CDATA[
			menuConnexion();
		// ]]>
	</script>
</body>
</html>