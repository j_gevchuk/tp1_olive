<?php ob_start(); ?>
<!DOCTYPE html 
     PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	
<html xmlns="http://www.w3.org/1999/xhtml">
	<link href="../css/css.css" rel="stylesheet" type="text/css" />
		
		<head>
			<!-- Fonts et ASCII -->
			<?php
				include_once("php/fonts.htm");

			?>
			<title>Compte client - Les Oliviers Ould Hocine</title>
			<meta name="description" content="Page d'inscription, pour pouvoir avoir accès aux comptes personnels et passer des commandes" />
			<meta name="keywords" content="Compte, commande, inscription, oliviers, ould hocine" /> 
			<meta name="robots" content="index, follow" /> 
			<meta name="author" content="Lilia Ould Hocine" />
		
			<!-- Appels CSS et javascript -->
			<?php
				include_once("php/cssETjs.htm");
			?>
		</head>
		
<body class="corps">
	<div id="contenu">
		
		<!-- Entete -->
		<?php
			include_once("php/entete.php");
		?>
		
		
		<!-- Le menu -->
		<?php
			include_once("php/menu.htm");
		?>		
		
		<!-- Contenu pprincipal -->
		<div id="contenuPrincipal">	
			<?php
				$message = "";
				if((isset($_GET["message"])) && (isset($_SESSION["user"])))
				{
					if($_GET["message"] === "2")
					{
						echo "<h3 style='text-align:center;'>Votre commande a bien été enregistrée<br/>Nous vous contactons dans les plus brefs délais!</h3>";
					}
				}
				if(isset($_SESSION["user"]))
				{
					include_once("php/comptes.php");
				} else {
					include_once("php/inscription.php");
				}
			?>
		</div>		
		
		<!-- Bas de page -->
		<?php
			include_once("php/reseaux.htm");
			include_once("php/piedPage.htm");
		?>			
	</div>
	
	<?php
		include_once("php/jQuery.htm");
	?>
	<script  type="text/javascript">
		// <![CDATA[
			menuConnexion();
		// ]]>
	</script>
</body>
</html>
<?php ob_end_flush(); ?> 