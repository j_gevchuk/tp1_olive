<?php 
/**
 * Fichier: 'controleur_panier.php'
 * * @author  Iulianna Gevchuk
 * Description: classe Controleur_Panier
 *
 */


	class Controleur_Panier extends Controleur {
		private $modele;

		public function __construct(){
			parent::__construct();
			//Créér un objet Panier
			$this->modele = new Panier();
		}

		//Fonction est appelée lorsque l'utilisateur clique 'Ajouter produit au panier'
		public function ajouter(){
			//Vérifier si l'id du produit exista dans la BD et s'il n'est pas encore ajouté au panier
			if($this->modele->verifProduit($_GET['ajouter'], 1)){
				$this->vue->ajouter();
			}
			//Le produit n'existe pas ou bien est déjà ajouté
			else {
				$this->vue->erreur();
			}
		}

		//Afficher le panier lorsque l'utilisateur clique sur le lien Panier
		public function afficherPanier(){
			$this->vue->panier();
			//Supprimer le produit du panier
			if(isset($_GET['supprimer'])){
				$this->modele->supprimerArticle($_GET['supprimer']);
				$this->vue->panier();
			}
			//Augmenter la quantité du produit
			if(isset($_GET['augmenter'])){
				$this->modele->augmenterQuantite($_GET['augmenter']);
				$this->vue->panier();
			}
			//Diminuer la quantité du produit
			if(isset($_GET['diminuer'])){
				$this->modele->diminuerQuantite($_GET['diminuer']);
				$this->vue->panier();
			}
		}
	}

 ?>