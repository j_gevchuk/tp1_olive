<?php 
/**
 * Fichier: 'main_controleur.php'
 * * @author  Iulianna Gevchuk
 * Description: classe Controleur
 *
 */

//  LE PROBLÈME QUI N'EST PAS RÉSOLUE: L'ACTION AJOUTER, SUPPRIMER, AUGMENTER ET DIMINUER LA QUANTITÉ SE FAIT LORSQUE ON CLIQUE UNE DEUXIÈME FOIS???!!!

    class Controleur {
        public $vue;
        public $url;
        public $panier;

        public function __construct($url = ''){
            //Créér une instance Vues
            $this->vue = new Vues();
            $this->url = $url;
        }


        //Afficher un contenu selon le lien cliqué, j'ai modifié un peu les urls du menu...
        public function invoquer(){
            //Page 'index'
            $page;
            if($this->url == '' || !$this->url){
                $page = 'accueil';
            }
            //Tous autres cas
            else {
                $page = $this->url['page'];
            }
            switch($page){
            case 'accueil':
                $this->vue->accueil();
                break;
            case 'oliviers':
                $this->vue->oliviers();
                break;
            case 'produits':
                $modele= new Modele_Produits();
                $produits = $modele->getListeProduits();
                $this->vue->produits($produits);
            //La liste des produits affichés dynamicament
                if(isset($_GET['ajouter'])){
                    $panier = new Panier();
                    $panier->verifProduit($_GET['ajouter'], 1);
                }
                break;
            case 'panier':
                $panier = new Panier();
                $this->vue->panier();
                if(isset($_GET['supprimer'])){
                    $panier->supprimerArticle($_GET['supprimer']);
                }
                if(isset($_GET['diminuer'])){
                    $panier->diminuerQuantite($_GET['diminuer']);
                }
                if(isset($_GET['augmenter'])){
                    $panier->augmenterQuantite($_GET['augmenter']);
                }
                break;
            case 'contacts':
                $this->vue->contacts();
                break;
            case 'inscription':
                $controleur = new Controleur_Inscription();
                $controleur->enregistrer();
                break;
            case 'plan':
                $this->vue->planSite();
                break;
                //Un autre url
            default: 
                $this->vue->accueil();
                break;
            }
        }
    }
 ?>