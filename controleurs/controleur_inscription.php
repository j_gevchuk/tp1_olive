<?php 
/**
 * Fichier: 'Controleur_Inscription.php'
 * * @author  Olga Zlotea
 * Création: 18.06.2013
 * 
 * Description: Inscription d'un novel utilisateur au site
 */
include_once ('modeles/inscription.php');
	class Controleur_Inscription  extends Controleur {
	private $modele;

	function __construct(){
		parent::__construct();
		//Instancir un modèle qui gère l'inscription
		$this->modele = new Inscription();
	}

	//Ajout de nouveux utilisateurs à la BD
	public function enregistrer(){
		//Inclure la forme à remplir
		include_once 'vues/Fiche_Oho.php';
		//Cliquer sur le bouton 'Enregistrer'
		if(isset($_POST['sbmInscription'])) {
			//Vérifier si les champ 'Nom' et 'Mot de passe' sont remplis
			if($_POST['txtcCourriel'] != '' && $_POST['txtsPassInsc'] != '' && $_POST['txtsPassInsc2'] != '' && $_POST['txtsNom'] != '' && $_POST['txtsPrenom'] != '' && $_POST['txtnTel'] != '' && $_POST['txtnAdresse'] != ''){
				$login = $_POST['txtcCourriel'];
				$password = $_POST['txtsPassInsc'];
				$nomClient = $_POST['txtsNom'];
				$prenomClient = $_POST['txtsPrenom'];
				$telephon = $_POST['txtnTel'];
				$adresse = $_POST['txtnAdresse'];
				//Appeler la méthode du Modèle et passer les paramètres
				if($this->modele->ajouterClient($login,$password,$prenomClient,$nomClient,$telephon,$adresse)){
					regUser($login);
				}
			}
			//Si un des champ requis est vide, afficher le message d'erreur
			else {
				include_once 'vues/error.php';
				inscriptionError();
			}
		}
	}
}
 ?>