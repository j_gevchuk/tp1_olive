<?php 
/**
 * Fichier: 'accessbd.php'
 * * @author  Iulianna Gevchuk
 * Description: classe AccessBD qui fourni l'accès à la BD
 *
 */

class AccessBD {
	private static $HOST = 'localhost';
    private static $USER = 'root';
    private static $PWD = '';
    private static $BDD = 'oliv';

	//Fonction pour se connecter à la base de donnée
	public static function connecter()
    {
        $connection = new PDO('mysql:host=' . self::$HOST . ';dbname=' . self::$BDD, self::$USER, self::$PWD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8"));
        $connection->setAttribute(PDO::ATTR_ERRMODE ,PDO::ERRMODE_EXCEPTION ); 
        return $connection;
    }
	
	//Fonction pour récupérer des données de la base de donnée
	public static function select($connexion, $sReq, $aVars = array()){
		try {
			$resultats = $connexion->prepare($sReq);
			$resultats->execute();
			$resultats->setFetchMode(PDO::FETCH_OBJ);
		} 
		catch (PDOException $e) {
            die("Erreur ! : " . $e->getMessage());
        }
        return $resultats;
	}
}
 ?>