<?php 
/**
 * Fichier: 'panier.php'
 * * @author  Iulianna Gevchuk
 * Description: classe Panier qui permet de gèrer le panier
 *
 */
	class Panier {
		private $connexion;

		public function __construct(){
			//Créér une connexion à la BD
			$this->db = new AccessBD();
			$this->connexion = $this->db->connecter();
			//Ouvrir la session 'panier' si elle n'existe pas
			if(!isset($_SESSION['panier'])){
				$_SESSION['panier'] = array();
			}
		}

		//Vérifier si le produit existe dans la BD et s'il n'est pas encore dans le panier
		public function verifProduit($id, $quantite = 1){
			//Faire une requête à la BD
			$requete = "SELECT * FROM produits WHERE idProd = '$id'";
			$produit = $this->db->select($this->connexion, $requete);
			$produit = $produit->fetch();
			// //Le produit n'existe pas
			if(empty($produit)){
				return;
			}
			//Le produit est déjà ajouté
			if(!$this->produitsAjoutesPanier($id)){
				$this->ajouter($id);
			}
		}

		//Ajouter le produit au panier et garder les données dans la session
		public function ajouter($id){
			$requete = "SELECT * FROM produits WHERE idProd = '$id';";
			$resultats = $this->db->select($this->connexion, $requete);
			$resultat = $resultats->fetch();
			$_SESSION['panier'][$id]['quantite'] = 1;
			$_SESSION['panier'][$id]['nom'] = $resultat->nom;
			$_SESSION['panier'][$id]['description'] = $resultat->description;
			$_SESSION['panier'][$id]['prix'] = $resultat->prix;
			$_SESSION['panier'][$id]['url_media'] = $resultat->url_media;
		}

		//Fonction parcour le panier et retourne false si l'article est déjà ajoutée
		public function produitsAjoutesPanier($idProd){
			//Récupèrer les clés du panier (les ids des produits)
			$ids = array_keys($_SESSION['panier']);
			$flag = false;
			foreach ($ids as $id) {
				//Retourner un false si l'article est déjà au panier
				if($id == $idProd){
					$flag = true;
					break;
				}
			}
			return $flag;
		}

		//Supprimer l'article
		public function supprimerArticle($id){
			unset($_SESSION['panier'][$id]);
		}

		//Augmenter la quantite de l'article
		public function augmenterQuantite($id){
			$_SESSION['panier'][$id]['quantite']++;
		}

		//Diminuer la quantite 
		public function diminuerQuantite($id){
			//Si la quantité est 1 et moins, on supprime le produit du panier
			if($_SESSION['panier'][$id]['quantite'] <= 1){
				$this->supprimerArticle($id);
			}
			else {
				$_SESSION['panier'][$id]['quantite']--;
			}
		}
	}

 ?>