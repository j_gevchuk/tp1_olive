<?php 
/**
 * Fichier: 'modele_produit.php'
 * * @author  Iulianna Gevchuk
 * Description: classe Modele_Produits qui récupère des données de la BD
 *
 */


class Modele_Produits {

	//Fonction qui récupère des produits de la base de données 
	public function getListeProduits(){
		$db = new AccessBD();
		$connexion = $db->connecter();
		//Afficher les articles disponibles
		$requete = 'SELECT * FROM produits WHERE disponibilite = 1';
		$resultats = $db->select($connexion, $requete);
		return $resultats;
	}
}
 ?>
