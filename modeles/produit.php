<?php 
/**
 * Fichier: 'produit.php'
 * * @author  Iulianna Gevchuk
 * Description: classe Produit
 *
 */

class Produit {
	private $id;
	private $nom;
	private $description;
	private $prix;
	private $url_media;	
	private $disponible;	

		public function __construct($id,$nom,$description, $prix,$url_media, $disponible){
			$this->id = $id;
			$this->nom = $nom;
			$this->description = $description;
			$this->prix = $prix;
			$this->url_media = $url_media;
			$this->disponible = $disponible;
			
		}

		public function getId(){
			return $this->id;
		}

		public function getNom(){
			return $this->nom;
		}
		public function setNom($nom){
			$this->nom = $nom;
		}
		public function getDescription(){
			return $this->description;
		}
		public function setDescription($description){
			$this->description = $description;
		}
		public function getPrix(){
			return $this->prix;
		}
		public function setPrix($prix = null){
			$this->prix = $prix;
		}
		public function getUrl_media(){
			return $this->url_media;
		}
		public function setUrl_media($url_media = null){
			$this->url_media = $url_media;
		}
		public function getDisponibilite(){
			return $this->disponible;
		}
		public function setDisponibilite($disponible = 1){
			$this->disponible = $disponible;
		}

	
}
 ?>