-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 30, 2013 at 07:12 PM
-- Server version: 5.5.27-log
-- PHP Version: 5.4.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `oliv`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `emailAdmin` varchar(40) NOT NULL,
  `passwordAdmin` varchar(20) DEFAULT NULL,
  `nameAdmin` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`emailAdmin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`emailAdmin`, `passwordAdmin`, `nameAdmin`) VALUES
('gevchuk@gmail.com', 'abc', 'Julianna Gevchuk'),
('olga.zlotea.web@gmail.com', '123', 'Olga Zlotea');

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE IF NOT EXISTS `client` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `login` varchar(20) DEFAULT NULL,
  `PasswordClient` varchar(20) DEFAULT NULL,
  `nomClient` varchar(20) DEFAULT NULL,
  `prenomClient` varchar(20) DEFAULT NULL,
  `telephone` varchar(50) DEFAULT NULL,
  `pays` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `facture`
--

CREATE TABLE IF NOT EXISTS `facture` (
  `commande` int(20) NOT NULL AUTO_INCREMENT,
  `id` int(20) NOT NULL,
  PRIMARY KEY (`commande`,`id`),
  KEY `FK_Facture_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `panier`
--

CREATE TABLE IF NOT EXISTS `panier` (
  `commande` int(20) NOT NULL AUTO_INCREMENT,
  `quantite` int(10) DEFAULT NULL,
  `idProd` int(20) NOT NULL,
  PRIMARY KEY (`commande`),
  KEY `FK_Panier_idProd` (`idProd`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `produits`
--

CREATE TABLE IF NOT EXISTS `produits` (
  `idProd` int(20) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `prix` float DEFAULT NULL,
  `url_media` varchar(40) DEFAULT NULL,
  `disponibilite` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`idProd`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `produits`
--

INSERT INTO `produits` (`idProd`, `nom`, `description`, `prix`, `url_media`, `disponibilite`) VALUES
(2, 'Lait démaquillant douceur', 'Lait pour le visage,150ml', 12, 'medias/images/produits/2.jpg', 1),
(3, 'Gel lissant yeux & lèvres', 'Soins,15ml', 36, 'medias/images/produits/3.jpg', 1),
(4, 'Soin douceur jour', 'Soin,50ml', 34, 'medias/images/produits/4.jpg', 1),
(5, 'Crème douceur exfoliante', 'Crème pour le corp, 200ml', 32, 'medias/images/produits/5.jpg', 1),
(6, 'Huile de beauté', 'Huile pour le corp, 500ml', 68, 'medias/images/produits/6.jpg', 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `facture`
--
ALTER TABLE `facture`
  ADD CONSTRAINT `FK_Facture_commande` FOREIGN KEY (`commande`) REFERENCES `panier` (`commande`),
  ADD CONSTRAINT `FK_Facture_id` FOREIGN KEY (`id`) REFERENCES `client` (`id`);

--
-- Constraints for table `panier`
--
ALTER TABLE `panier`
  ADD CONSTRAINT `FK_Panier_idProd` FOREIGN KEY (`idProd`) REFERENCES `produits` (`idProd`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
