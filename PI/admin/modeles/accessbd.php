<?php 
class AccesBD {
	private static $HOST = 'localhost';
    private static $USER = 'root';
    private static $PWD = '';
    private static $BDD = 'oliv';

	//Fonction pour se connecter à la base de donnée
	public static function connecter()
    {
        $connection = new PDO('mysql:host=' . self::$HOST . ';dbname=' . self::$BDD, self::$USER, self::$PWD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES UTF8"));
        $connection->setAttribute(PDO::ATTR_ERRMODE ,PDO::ERRMODE_EXCEPTION ); 
        return $connection;
    }
	
	//Fonction pour récupérer des données de la base de donnée
	public static function preparer($connexion, $sReq, $aVars = null){
		try {
			$resultats = $connexion->prepare($sReq);
			$resultats->execute();	

		} catch (PDOException $e) {
            die("Erreur ! : " . $e->getMessage());
        }
		return $resultats;
	}
	
	//Fonction pour récupérer des données de la base de donnée
	public static function preparerID($connexion, $sReq, $aVars){
		try {
		
			$resultats = $connexion->prepare($sReq);
			$resultats->execute($aVars);	

		} catch (PDOException $e) {
            die("Erreur ! : " . $e->getMessage());
        }
		return $connexion->lastInsertId();;
	}

	//Fonction pour récupérer des données de la base de donnée
	public static function select($connexion, $sReq){
		try {

			$resultats=$connexion->query($sReq);
			$resultats->setFetchMode(PDO::FETCH_OBJ);

		} catch (PDOException $e) {
            die("Erreur ! : " . $e->getMessage());
        }
		return $resultats;
	}
}
 ?>