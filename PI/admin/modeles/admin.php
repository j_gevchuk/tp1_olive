<?php 
class Admin {
	private $mail;
	private $passw;
	private $nom;
	
		public function __construct($mail,$passw,$nom){
			$this->mail = $mail;
			$this->passw = $passw;
			$this->nom = $nom;
		}

		public function getMail(){
			return $this->mail;
		}
		public function setMail($mail){
			$this->mail = $mail;
		}
		public function getPassw(){
			return $this->passw;
		}
		public function setPassw($passw){
			$this->passw = $passw;
		}
		public function getNom(){
			return $this->nom;
		}
		public function setNom($nom){
			$this->nom = $nom;
		}
	
}
 ?>