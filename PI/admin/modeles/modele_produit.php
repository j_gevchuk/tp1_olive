<?php 
/**
 * Fichier: 'modele_produit.php'
 * * @author Olga Zlotea
 * Création: 19.06.2013
 * 
 * Description: Class Modele produit 
 */
include_once('accessbd.php');
include_once('modeles/produit.php');
class Modele_Produits {
	private $db;
	private $idProd;
	//Instancier le modèle
	public function __construct(){
		$this->db = new AccesBD();
	}
	//Fonction qui récupère des produits de la base de données 
	public function getListeProduits(){
		$connexion = $this->db->connecter();
		//Afficher les produits disponibles
		$requete = 'SELECT * FROM produits WHERE disponibilite = 1';
		$resultats = $this->db->preparer($connexion, $requete);
		$aProduits = array();
		//Créér un tableau des objets
		while($resultat = $resultats->fetch()){
			$produit = new Produit($resultat['idProd'],
									$resultat['nom'],
									$resultat['description'],
									$resultat['prix'],
									$resultat['url_media'],
									$resultat['disponibilite']);
			$aProduits[] = $produit;
		}
		//Retourner un tableau des objets au controleur
		return $aProduits;
	}

	//Méthode prend un ID du produit (clé primaire) comme un paramètre et le supprime dans la BD
	public function supprProduits($id){
		//la connexion établie
			$connexion = $this->db->connecter();
			////Chercher et supprimer une article par son ID 
			$requete = "DELETE FROM produits WHERE idProd = '$id'";
			$resultats = $this->db->preparer($connexion, $requete);
			//$resultat = $resultats->fetch(PDO::FETCH_ASSOC);
			return $resultats;
	}

	//Méthode retourne un produit trouvé dans la BD par son ID passé au paramètre 
	public function getProduit($id){
		//la connexion établie
		$connexion = $this->db->connecter();
		if($connexion){
			//Sélectionner un article par son ID
			$requete = "SELECT * FROM produits WHERE idProd = '$id'";
			$resultats = $this->db->select($connexion, $requete);
			return $resultats;
		}
	}

	//Method prend un ID du produit comme un paramétre pour modifier ce produit
	public function modifierProduit($id,$nom,$description, $prix,$url_media, $disponible){
		//la connexion établie
		$produit = new Produit($id,$nom,$description,$prix,$url_media,$disponible);
		$connexion = $this->db->connecter();
		if($connexion){
			//Sélectionner un article par son ID
			$requete = "UPDATE produits 
						SET nom = '$nom',
						description='$description',
						prix='$prix',
						url_media='$url_media',
						disponibilite='$disponible'

						WHERE idProd = '$id'";
			$resultats = $this->db->preparer($connexion, $requete);
			//Exécuter la requête
			if($resultats->execute()){
				return true;
			}
			else {
				return false;
			}
		}
		else {
			echo 'Erreur lors de la sélection de la base de données.';
		}
	}
	//Method ajoute un produit a la bd
	public function ajouterProduit($nom, $description,$prix,$url_media,$disponibilite){
		//Si la connexion établie
		$connexion = $this->db->connecter();
		if($connexion){
			//Sélectionner un article par son ID
			$requete = "INSERT INTO produits (nom, description, prix, url_media, disponibilite) VALUES ('$nom', '$description', '$prix', '$url_media', '$disponibilite');";
			$resultats = $this->db->preparer($connexion, $requete);
				return $resultats;
		}
	}
}


 ?>
