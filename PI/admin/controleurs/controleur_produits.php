<?php 
/**
 * Fichier: 'Controleur_Produits.php'
 * * @author Olga Zlotea
 * Création: 19.06.2013
 * 
 * Description:method appele a class Modele_Produits pour afficher les produits 
 */
include_once 'modeles/modele_produit.php';
	//Class controleur de produits
	class Controleur_Produits{
		private $produit;
		//Method aficher les produits de bd
		public function produits(){
			$this->modele = new Modele_Produits();
		 	//Appeler la méthode qui récupère les produits de la BD
		 	$produits = $this->modele->getListeProduits();
		 	//Affciher dans un format HTML
		 	$vue = new Vues();
		 	$vue->produits($produits);
	 	}
	 	//Method supprimer un produit de bd
	 	public function supprimerProduits(){
	 		$this->modele = new Modele_Produits();
		 	//Appeler la méthode qui récupère les produits de la BD
		 	$res = $this->modele->supprProduits($_POST['id']);
		 	if($res){
		 		$produits =$this->modele->getListeProduits();
		 		$vue = new Vues();
		 		$vue->produits($produits);
		 	}
	 	}
	 	//Method select un produit de bd avec IdProduit
	 	public function selectProduit(){
	 		$this->modele = new Modele_Produits();
			$id = $_GET['idProd'];
			$produit = $this->modele->getProduit($id);
			$vue = new Vues();
			$vue->formProduits($produit);
		}
		//Method modifier un produit
		public function modifierProd(){
			$this->modele = new Modele_Produits();
			$vue = new Vues();
			$vue->formModProd();
			if($_POST['enregistrer']) {
				$id = $_POST['txtsId'];
				$nom = $_POST['txtsLogin'];
				$description = $_POST['txtsDescr'];
				$prix = $_POST['txtsPrix'];
				$url_media = $_POST['txtsUrl'];
				$disponible = $_POST['txtsDisp'];
				$res = $this->modele->modifierProduit($id,$nom,$description, $prix,$url_media, $disponible);
				if($res){
					$produits =$this->modele->getListeProduits();
			 		$vue = new Vues();
			 		$vue->produits($produits);
				}
			}
		}

		//Method qui ajout un produit à la bd
		public function ajouterProduit(){
			$this->modele = new Modele_Produits();
			if($_POST['enregistrerProd']){
				$nom = $_POST['txtsLogin'];
				$description = $_POST['txtsDescr'];
				$prix = $_POST['txtsPrix'];
				$url_media = $_POST['txtsUrl'];
				$disponibilite = $_POST['txtsDisp'];
			}
			$produit = $this->modele->ajouterProduit($nom, $description,$prix,$url_media,$disponibilite);
			if($produit){
				$produits = $this->modele->getListeProduits();
				$vue = new Vues();
				$vue->produits($produits);
			}
		}

	}
 ?>
