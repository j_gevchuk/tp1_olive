<?php 
/**
 * Description de la classe Controleur
 *
 * @author Olga Zlotea
 */
//Inclusion du fichier de la class Controleurs

include_once ('vues/vues.php');
include_once('controleurs/controleur_admin.php');
include_once('controleurs/controleur_produits.php');

 class Loader {
 	public $vues;
  public $url;
  public $page;
  //public $entree;
   
 	//Instancier le modèle
 	public function __construct($url = ''){
 		$this->vues = new Vues();
    $this->url = $url;
 	}

 	//Appeler les modèles et vues selon options choisies par l'utilisateur
 	public function invoquer(){
    //Si n'est pa la session Admin,ouvrier la session Admin
     if(!isset($_SESSION['admin'])){
            $_SESSION['admin'] = array();
        }
        //Connexion Admin
         if (isset($_POST['sbmConnexion'])){
            $admin = new Controleur_Admin();
            $admin->admin();
        }
      //Page 'index'
            $page;
            if($this->url == '' || !$this->url){
                $this->vues->accueil();
            }
            //Tous autres cas
            else {
                $this->page = $this->url['page'];
            }

        
        switch($this->page){
          //Pge Gerer les produitsS
          case '3':
             $controleur = new Controleur_Produits;
             $controleur->produits();
          break;
          //Cas supprimer le produit
          case 'id':  
                $controleur = new Controleur_Produits();
                $controleur->supprimerProduits();
          break;
          //Cas ajouter le produit
          case 'ajoutProd':
            //Affichage le list de produits apre ajouter le nouveau produit
              if(isset($_POST['enregistrerProd'])){
                  $controleur = new Controleur_Produits();
                  $controleur->ajouterProduit();
              } else {
                //Formulaire pour ajouter le produit
                $this->vues->formAjouterProduit();
                }
          break;
          //Cas modifier le produit
          case 'modif':
              //Formulaire avec les information de produit
                $controleur = new Controleur_Produits();
                $controleur->selectProduit();
                //Enregistrement les changements de produits dans la bd et affichage tout les produits
                if(isset($_POST['enregistrer'])){
                $controleur = new Controleur_Produits();
                $controleur->modifierProd();
               } 
          break;
          //Fermer la session de Admin
          case 'deconexion':
                $this->vues->accueil();
          break;

        } 

    } 
 }  

 
 ?>         
      
 		






        
 