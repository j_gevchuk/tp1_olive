<?php 
/**
 * Fichier: 'Controleur_Admin.php'
 * * @author Olga Zlotea
 * Création: 19.06.2013
 * 
 * Description:Class Controleur Admin
 */
include_once 'modeles/modele_admin.php';
	class Controleur_Admin extends Loader{
		private $modele;
		private $vue;
		//Instancier le modèle
		 function __construct(){
			$this->modele = new Modele_Admin();
			$this->vue = new Vues();
		} 	
		//Method connexion Admin
		public function admin(){
			//Valeur du champ Nom d'utilisateur
            $login = $_POST['txtsLogin'];
            //Valeur du champ Mot de passe
             $pass = $_POST['txtsPass'];
		 	//Appeler la méthode qui récupère les produits de la BD
		 	$this->modele->getAdmin($login,$pass);
		 	//Si la session de l'admin est ouverte, le menu menu s'affichera
		 	if(isset($_SESSION['admin'])){
				$this->vue ->afficherAdmin();
		 	}
			else{
				//Afficher le message d'erreur
				$this->vue->erreur();
			}
	 	}
	}
 ?>
