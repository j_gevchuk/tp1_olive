<!DOCTYPE html 
 PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta charset='utf-8'/>
			<!-- Fonts et ASCII -->
			<?php
				//include_once("vues/fonts.htm");
			?>
			<title>Contact - Les Oliviers Ould Hocine</title>
			<meta name="description" content="Contactez les Oliviers Ould Hocine, nous sommes à votres dispositions pour toute auestion commande ou recommandation sur l'huile d'olive et ses amis" />
			<meta name="keywords" content="contact, commande, questions, Olive, Huile, Algérie, Tradition, Ould Hocine, Bouira, Oliviers" /> 
			<meta name="robots" content="index, follow" /> 
			<meta name="author" content="Lilia Ould Hocine" />
		
			<!-- Appels CSS et javascript -->
			<?php
				include_once("vues/cssETjs.htm");
			?>
		</head>
		
<body class="corps">
	<div id="contenu">
		
		<!-- Entete -->
		<?php
			//include_once("vues/php/entete.php");
		?>
		<div id="contenuPrincipal">
			<!--Affichage le menu Admin-->
			<div class="menuAdmin">
				<?php
					include_once('vues/session_admin.php');
				?>
			</div>
			<?php 
				include_once('vues/menuAdmin.htm');
			 ?>
			 <div class="produit">
			 
			 <?php 
				include_once('vues/formProduit.php');
			 ?>
			 </div>
		</div>			
	</div>
	
</body>
</html>

