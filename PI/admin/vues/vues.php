<?php 
	class Vues{
		public function accueil(){
			include_once('vues/header.php');
		}
		public function admins(){
			include_once('vues/admin.php');
		}
		public function afficherAdmin() {
			include_once('vues/afficherAdmin.php');
		}
		public function produits($produits){
			include_once('vues/gererProduits.php');
		}
		public function erreur(){
			include_once 'js/erreur.html';
		}
		public function formProduits($produit){
			include_once('vues/afficherFormProduit.php');
		}
		public function formAjouterProduit(){
			include_once('vues/afficherformAjoutProd.php');
		}
		public function formModProd(){
			include_once('vues/formProduit.php');
		}

	}
 ?>